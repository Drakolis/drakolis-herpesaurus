Application -- drakolis-harpesaurus itself. Placed in the src/App.vue. Should only control module display and OS functions.

Module -- Functionaly independent parts of the Application (Mail, Calendar, ToDo, Journal etc). Composed from one or several Views. Placed in modules.

View -- Main view representation. There are usually several views which display the main entity of the module. Placed in components. More or less main workspaces of the Application.

Modal -- Drawer based subviews controlled from one place. Only one can be open at each time. Only should give it's contents. Everything else is managed by ModalController component.

Buttons -- Allow to do common actions on Modals or Views. Repeatable buttons SHOULD BE SEPERATED AND GENERALISED

Dialogs -- Confirmation flyouts. Should only be available in Views or Modals (or on destructive action Buttons)
