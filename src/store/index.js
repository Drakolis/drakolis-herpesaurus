import Vue from 'vue';
import Vuex from 'vuex';
import { CALENDAR_EVENT_POPUP_MAX_WIDTH } from '../constants/ElementSizes';
import { CALENDAR_EVENT_EDITOR_MODAL } from '../constants/Modals';

Vue.use(Vuex);

const defaultModal = {
  type: null,
  props: null,
};

export default new Vuex.Store({
  state: {
    // Settings
    settings: {
      calendar: {
        display: {
          weeksCount: 6, // Every month will fit in 6 weeks
          weekDaysDisplay: [0, 1, 2, 3, 4, 5, 6],

          weekDaysWeekend: [5, 6],
          showWeekend: true,

          formatForDay: 'DD',
          formatForMonthDay: 'DD MMM',
          formatForYearDay: 'DD MMM YY',
          formatForTime: 'LT',
          formatForEventDetails: 'LLLL',
          formatForLongEventDetails: 'llll',
        },
      },
    },

    // Modals
    activeModal: defaultModal,
  },
  mutations: {
    // Modals
    setActiveModal: (state, activeModal) => {
      state.activeModal = activeModal;
    },
  },
  actions: {
    // Modals
    closeModals: (context) => {
      context.commit('setActiveModal', defaultModal);
    },
    openEventEditorModal: (context, event) => {
      context.commit('setActiveModal', {
        type: CALENDAR_EVENT_EDITOR_MODAL,
        props: { event },
      });
    },
  },
  modules: {
  },
  getters: {
    // Settings
    calendarDisplaySettings: (state) => state.settings.calendar.display,
    calendarDisplayConstants: () => ({
      popupMaxWidth: Math.max(500, CALENDAR_EVENT_POPUP_MAX_WIDTH),
    }),

    // Modals
    eventEditorModal: (state) => state.activeModal.type === CALENDAR_EVENT_EDITOR_MODAL
      && state.activeModal.props,
  },
});
