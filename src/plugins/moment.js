import Vue from 'vue';
import vueMoment from 'vue-moment';
import moment from 'moment';
import 'moment/locale/el';

moment().localeData().ordinal();

moment.locale('el', {
  week: {
    dow: 1,
  },
});

Vue.use(vueMoment, { moment });

console.log(Vue.moment().locale());
